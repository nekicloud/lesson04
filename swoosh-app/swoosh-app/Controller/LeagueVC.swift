//
//  LeagueVC.swift
//  swoosh-app
//
//  Created by luka on 23/12/2018.
//  Copyright © 2018 luka. All rights reserved.
//

import UIKit

class LeagueVC: UIViewController {
    
    var player: Player!

    @IBOutlet weak var nextBtn: buttonBorder!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        player = Player()

    }
    

    @IBAction func onNextTap(_ sender: Any) {
        performSegue(withIdentifier: "skillVCSegue", sender: self)
    }
    
    @IBAction func onMensTap(_ sender: Any) {
        selectLeague(leagueType: "mens")
    }
    
    
    @IBAction func onWomensTap(_ sender: Any) {
        selectLeague(leagueType: "womens")


    }
    
    @IBAction func onCoedTap(_ sender: Any) {
        selectLeague(leagueType: "coed")


    }
    
    func selectLeague(leagueType: String) {
        player.desiredLeague = leagueType
        nextBtn.isEnabled = true
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?){
        if let skillVC = segue.destination as? SkillVC {
            skillVC.player = player
        }
    }

}
