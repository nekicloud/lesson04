//
//  buttonBorder.swift
//  swoosh-app
//
//  Created by luka on 22/12/2018.
//  Copyright © 2018 luka. All rights reserved.
//

import UIKit

class buttonBorder: UIButton {

    override func awakeFromNib() {
        super.awakeFromNib()
        layer.borderWidth=2.0
        layer.borderColor=UIColor.white.cgColor
    }

}
